import { Component, OnDestroy } from '@angular/core';
import { Service } from '../../services/service';
import { LoginComponent } from 'src/app/login/login.component';




@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnDestroy {

  title = 'UserDetail';
  success: boolean = false;

  ngOnDestroy() {
    localStorage.removeItem('IsLoggedIn');
    localStorage.removeItem('IsLoggedFb');
    localStorage.removeItem('IsLoggedgoogle');
  }

  get User(): any {
    this.success = true;
    return localStorage.getItem('IsLoggedIn');

  }

  get UserFb(): any {
    return localStorage.getItem('IsLoggedFb');
  }

  get UserGoogle(): any {
    return localStorage.getItem('IsLoggedgoogle');
  }

  logout() {
    // Remove data from localStorage

    this.ngOnDestroy();
    this.success;
  }
}





