import { Component } from '@angular/core';

@Component({
    selector: 'userdetail',
    templateUrl: 'userdetail.component.html'
})

export class UserDetailComponent {
    value: any = '';

    get user(): any {
        return localStorage.getItem('IsRegister');
    
      }
    
      add() {
    
        this.value = this.user;
    
      }
      delete() {
        this.value = 'User is deleted';
        console.log("removed", this.value);
      }
    
}