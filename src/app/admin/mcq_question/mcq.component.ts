import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';




@Component({
  selector: 'mcq',
  templateUrl: './mcq.component.html',
  styleUrls: ['./mcq.component.css']
})
export class McqComponent implements OnInit {
value:any;
  message: string = '';
  Users: any = '';
  result: any = '';
  profileForm = new FormGroup({
  msgB: new FormControl('', Validators.required)

  });
  get msgB(): any { return this.profileForm.get('msgB'); }


  constructor(public snackBar: MatSnackBar) {



  }

  ngOnInit() {
    let val= localStorage.getItem('question');
    this.value=JSON.parse(val);
    console.log("value",this.value);
  }


  post() {

    this.result = this.profileForm.value;
    console.log(this.result)
    localStorage.setItem("answer", this.msgB.value);
    this.snackBar.open('Successfully Submitted', 'Ok', {
      duration: 10000,
      verticalPosition: 'top'
    });



  }
  Question(): any {
    // let val= localStorage.getItem('question');
    // this.value=JSON.parse(val);
    // console.log("value",this.value);
  }

}