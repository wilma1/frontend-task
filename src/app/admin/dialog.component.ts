import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Service } from './../services/service';

@Component({
  selector: 'pop-dialog',
  templateUrl: './dialog.component.html',
  //   styleUrls: ['./dialog.component.scss']
})
export class DialogComponent {

  value: any = '';
  result: any;
  val: any;
  profileForm = new FormGroup({
    Question: new FormControl('', Validators.required)

  });
  // get msgB(): any { return this.profileForm.get('msgB'); }

  public DialogToShow: String;
  constructor(
    public dialogRef: MatDialogRef<DialogComponent>, public dialogRef1: MatDialogRef<DialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
    public dataService: Service) {
    this.DialogToShow = data.dialog;
  }
  ngOnInit() {
    console.log(this.DialogToShow)
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  post() {
    console.log("post called")
    this.result = this.profileForm.value;

    console.log(this.result);
    this.dataService.saveUser(this.result);

    // localStorage.setItem("question", this.result);


    let formData = this.profileForm.value;
    var params = {
      question: formData.Question,

    }
    console.log(params, "before object")
    //  localStorage.setItem("question", params.question);
  }




}