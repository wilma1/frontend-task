import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { DemoMaterialModule } from '../shared/material-module';
import { CommonModule } from '@angular/common';  
import { ReactiveFormsModule } from '@angular/forms';


import { AdminRoutes } from './admin.routing';


import {
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule
} from '@angular/material';

import { UserManagementComponent } from '../admin/user_management/user.management.component';
import { McqComponent } from '../admin/mcq_question/mcq.component';
import { DialogComponent } from './dialog.component';
import {AdminComponent} from './admin.component';
import {UserDetailComponent} from './userdetail/userdetail.component';


@NgModule({
    imports: [
       
        FormsModule,
        HttpClientModule,
        DemoMaterialModule,
        MatFormFieldModule,
        RouterModule.forChild(AdminRoutes),
        MatIconModule,
        CommonModule,
        ReactiveFormsModule  
       
    ],
    declarations: [
        UserManagementComponent,
        McqComponent,
        DialogComponent,
        AdminComponent,
        UserDetailComponent
       
    ],
    exports: [
        MatFormFieldModule
    ],
    providers:[],

    entryComponents:[DialogComponent],    
    
    bootstrap: [UserManagementComponent]
})

export class AdminModule { }