import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserManagementComponent } from './user_management/user.management.component';
import { McqComponent } from './mcq_question/mcq.component';
import { AdminComponent } from './admin.component';
import { UserDetailComponent } from './userdetail/userdetail.component';
import { Service } from '../services/service';

// import { AuthGuard } from './_guards/index';

export const AdminRoutes: Routes = [{
  path: '', redirectTo: '/',
  pathMatch: 'full'
},

{
  path:'adminapp',
  component:AdminComponent,canDeactivate: [Service] 
},

{
  path: 'user',
  component: UserManagementComponent
},

{
  path: 'mcq',
  component: McqComponent
},
{
  path:'userdetail',
  component:UserDetailComponent
},

// otherwise redirect to home
{ path: '**', redirectTo: '' }];


