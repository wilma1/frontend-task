import { Component, OnDestroy, Input, OnInit, SimpleChanges, OnChanges } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DialogComponent } from '../dialog.component';
import { Service } from './../../services/service';


@Component({
  selector: 'user',
  templateUrl: './user.management.component.html',
  styleUrls: ['./user.management.css']
})
export class UserManagementComponent implements OnInit, OnDestroy, OnChanges {
  value: any = '';
  @Input() data;
  question: any = '';



  dataSource = DialogComponent;


  result: any;
  profileForm: any;
  finalUsers: any;
  constructor(public dialog: MatDialog, public service: Service) {
    this.finalUsers = this.service.getUsers();
    console.log(this.finalUsers);


  }
  ngOnInit() {
    this.question = this.data;
    console.log(this.question);
  }
  ngOnChanges(changes: SimpleChanges) {
    console.log(JSON.stringify(changes));
  }

  addQuestion(): void {
    const dialogRef = this.dialog.open(DialogComponent,
      {
        // width: '400px',
        // height: '300px',
        width: '250px',
        data: { question: this.dataSource }
      });

    dialogRef.afterClosed().subscribe(result => {
      this.profileForm = result;


    });

  }

  post() {
    console.log("posting")
    this.result = this.finalUsers;
    localStorage.setItem("question", JSON.stringify(this.result));




  }

  removeQuestion() {

    this.question = this.finalUsers.pop();
    console.log(this.question);
  }

  removeAt(index: number) {
    const question = this.finalUsers;
    question.splice(index, 1);

    this.finalUsers = question;
  }
  ngOnDestroy() {
    this.finalUsers;
  }

}