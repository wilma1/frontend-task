import { Component, AfterViewInit, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { Service } from '../services/service';
import { MatSnackBar } from '@angular/material';
import { AuthService, FacebookLoginProvider, GoogleLoginProvider } from 'angular-6-social-login';


@Component({
    selector: 'app-login',
    templateUrl: 'login.component.html',
    styleUrls: ['./login.component.css']
})

export class LoginComponent implements AfterViewInit, OnInit {
    ngAfterViewInit() { }
    Host: String;
    hide = true;
    user: any;
    profileForm: any;
    SocialUser: any = '';

    constructor(private route: ActivatedRoute, private router: Router, private socialService: AuthService, public http: HttpClient, public service: Service, public snackBar: MatSnackBar
    ) {
        this.profileForm = new FormGroup({
            username: new FormControl('', Validators.required),
            password: new FormControl('', Validators.required)

        });
        this.Host = this.service.host();


    }
    ngOnInit() {
        this.socialService.authState.subscribe((user) => {
            user = user;
            console.log(user);
        });
    }

    signInWithFB(): void {

        this.socialService.signIn(FacebookLoginProvider.PROVIDER_ID).then(
            (userData) => {
                console.log(" sign in data : ", userData);
                localStorage.setItem("IsLoggedFb", userData.name);
                this.router.navigate(["users"])
                this.snackBar.open('Successfully Created', 'Ok', {
                    duration: 10000,
                    verticalPosition: 'top'
                });
            }
        );
    }
    signInWithGoogle(): void {
        this.socialService.signIn(GoogleLoginProvider.PROVIDER_ID).then(
            (userData) => {
                console.log(" sign in data : ", userData);
                localStorage.setItem("IsLoggedgoogle", userData.name);
                this.router.navigate(["users"])
                this.snackBar.open('Successfully Created', 'Ok', {
                    duration: 10000,
                    verticalPosition: 'top'
                });
            }
        );
    }

    login() {
        let formData = this.profileForm.value;
        var params = {
            username: formData.username,
            password: formData.password
        }
        console.log(params, "before object")
        this.http.get(this.Host + "/userslogin").pipe().subscribe(val => {
            console.log(val, "login service");
            if (val[1].username == params.username && val[1].password == params.password) {
                localStorage.setItem("IsLoggedIn", params.username);
                this.router.navigate(["users"])
                // return this.params.username;
                // return this.profileForm.get('username');
                // console.log(params.username);
            }



        },
            error => {
                this.openSnackBar("Invalid Email or Password entered")
            }
        )
        if (params.username && params.password != this.adminlogin) {
            this.adminlogin;
            console.log(this.adminlogin);
            this.router.navigate(["adminapp"])
        }


    }

    params = {
        username: "",
        password: ""

    }
    register() {
        this.router.navigate(["register", this.params])
    }
    openSnackBar(message) {
        this.snackBar.open(message, "OK", {
            duration: 10000,
            verticalPosition: 'top'
        });
    }
    adminlogin() {
        // let formData = this.profileForm.value;
        var params = {
            username: "dsouza1",
            password: "dsouza"
        }
        console.log(params, "admin");
    }
}

