import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotComponent } from './forgot_pwd/forgot.component';
import { UserComponent } from './layouts/users/user.component';


const routes: Routes = [{
  path: '', redirectTo: '/',
  pathMatch: 'full'
},
{
  path: 'register',
  component: RegisterComponent
},
{
  path: 'login',
  component: LoginComponent,
},
{
  path: 'forgot',
  component: ForgotComponent
},
{
  path: 'users',
  component: UserComponent
},
{
  path: '',
  loadChildren: './admin/admin.module#AdminModule'
  // component:DashboardComponent
},
// otherwise redirect to home
{ path: '**', redirectTo: '' }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
