import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import 'hammerjs';
import { ReactiveFormsModule } from '@angular/forms';
import {
    SocialLoginModule,
    AuthServiceConfig,
    GoogleLoginProvider,
    FacebookLoginProvider,
} from "angular-6-social-login";


import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { DemoMaterialModule } from './shared/material-module';

import {
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule
} from '@angular/material';

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotComponent } from './forgot_pwd/forgot.component';
import { Service } from './services/service';
import { RegisterService } from './services/register.service';
import { UserComponent } from './layouts/users/user.component';
import {AuthService} from './services/auth_service.service';


export function getAuthServiceConfigs() {
    const config = new AuthServiceConfig(
        [
            {
                id: FacebookLoginProvider.PROVIDER_ID,
                provider: new FacebookLoginProvider('292326498093671')
            },
            {
                id: GoogleLoginProvider.PROVIDER_ID,
                provider: new GoogleLoginProvider('510055092376-4uan31975crjikfrrcmh3o3oalmnfrl0.apps.googleusercontent.com')
            }
        ]
    );
    return config;
}

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        AppRoutingModule,
        MatFormFieldModule,
        DemoMaterialModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        MatIconModule,
        SocialLoginModule
    ],
    declarations: [
        AppComponent,
        LoginComponent,
        RegisterComponent,
        ForgotComponent,
        UserComponent
    ],
    exports: [
        MatFormFieldModule
    ],
    providers: [Service, RegisterService,AuthService, {
        provide: AuthServiceConfig,
        useFactory: getAuthServiceConfigs
    }
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }