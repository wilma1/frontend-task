import { Component, AfterViewInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {FormControl, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.css']
})
export class ForgotComponent implements AfterViewInit{
    ngAfterViewInit(){}    
    constructor(public router:Router){
        
    }
    showForm:boolean=true;
    reset(){
        this.showForm=false;
    }
    register(){
        this.router.navigate(["register"])
    }
    login(){
        this.router.navigate([""])
    }
}
