import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, FormGroup, FormControl, AbstractControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Service } from '../services/service';
import { MatSnackBar } from '@angular/material';
import { RegisterService } from '../services/register.service';

@Component({
    selector: 'register',
    templateUrl: 'register.component.html',
    styleUrls: ['./register.component.css']
})

export class RegisterComponent {
    Host: string;
    user: any = {};
    registerForm: FormGroup;
    hide = true;


    constructor(
        private router: Router, public api: Service, private registerService: RegisterService, public http: HttpClient, public snackBar: MatSnackBar
    ) {
        this.registerForm = new FormGroup({
            Name: new FormControl('', [Validators.required]),
            UserName: new FormControl('', [Validators.required]),
            Address: new FormControl('', [Validators.required]),
            Email: new FormControl('', [Validators.required], this.validateEmailNotTaken.bind(this)),
            Contact: new FormControl('', [Validators.required]),
            Password: new FormControl('', [Validators.required, Validators.minLength(6)]),
            ConfirmPassword: new FormControl('', [Validators.required, Validators.minLength(6)])
        }, this.pwdMatchValidator);

        this.Host = this.api.host();

    }

    ngOnInit() {

    }
    pwdMatchValidator(frm: FormGroup) {
        return frm.get('Password').value === frm.get('ConfirmPassword').value
            ? null : { 'mismatch': true };
    }
    get password() { return this.registerForm.get('Password'); }
    get confirm_password() { return this.registerForm.get('ConfirmPassword'); }

    validateEmailNotTaken(control: AbstractControl) {
        return this.registerService.checkEmail(control.value).map(res => {
            return res ? null : { emailTaken: true };
        });
    }

    register() {
        let formData = this.registerForm.value;
        var params = {
            name: formData.Name,
            username: formData.UserName,
            address: formData.Address,
            password: formData.Password,
            email: formData.Email,
            contact: formData.Contact,
            confirmPassword: formData.ConfirmPassword
        }
        console.log(params, "before object")
        this.http.get(this.Host + "/users").pipe().subscribe(val => {
            console.log(val, "service");
            if (val[1].name == params.name && val[1].username == params.username && val[1].address == params.address && val[1].password == params.password && val[1].email == params.email && val[1].contact == params.contact && val[1].confirmPassword == params.confirmPassword) {
                localStorage.setItem("IsRegister", params.name);
                console.log('IsLoggedIn');
                this.router.navigate(["/login"])
                this.snackBar.open('Successfully Created', 'Ok', {
                    duration: 10000,
                    verticalPosition: 'top'
                });
            }
        })
    }
    login() {
        this.router.navigate(["login"])
    }
}
