import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { AuthService } from './services/auth_service.service';
import { Service } from './services/service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  params: string;
  // title = 'Angular';
  showNav: boolean = true;
  isLoggedIn$: Observable<boolean>;
  hideNav: boolean = false;

  constructor(private service: Service) { }

  ngOnInit() {
    // this.isLoggedIn$ = this.Service.isLoggedIn;
  }

  // onLogout(){
  //   this.authService.logout();                      
  // }

  // }

}
