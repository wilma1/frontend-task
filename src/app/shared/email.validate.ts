// import { AbstractControl } from '@angular/forms';
// import { RegisterService } from '../services/register.service';

// export class ValidateEmailNotTaken {
//     static createValidator(registerService: RegisterService) {
//         return (control: AbstractControl) => {
//             return registerService.checkEmail(control.value).map(res => {
//                 return res ? null : { emailTaken: true };
//             });
//         };
//     }
// }