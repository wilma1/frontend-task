import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/delay';
import { Service } from '../services/service';


@Injectable()
export class RegisterService {

    Host: string;
    constructor(private http: HttpClient, public api: Service) {
        this.Host = this.api.host();
    }

    checkEmail(email: string) {
        return this.http
            .get(this.Host + ' /users')
            .delay(1000)
            .map(res => {
                return Host => Host.filter(user => user.email === email)
                    .map(users => !users.length)
            });


    }

}