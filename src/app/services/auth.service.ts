import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Check_Auth } from './check_auth.service';

@Injectable({
    providedIn: 'root',
})

export class AuthGuard implements CanActivate {
    constructor(public check_auth: Check_Auth, public router: Router) { }
    canActivate() {
        if (this.check_auth.isLoggedIn())
            return true;
        else {
            this.router.navigate['login'];
            return false;
        }
    }
}