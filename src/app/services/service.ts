import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';



@Injectable({
    providedIn: 'root',
})


export class Service {

    visible: boolean;

    serverUrl: string = environment.serverUrl;
    res: any;
    users: any = [];
    constructor() {
        console.log(environment.serverUrl)
        this.visible = false;
    }

    host() {
        return this.serverUrl;
    }
    getUser(): any {
        return localStorage.getItem('IsLoggedIn');
    }
    saveUser(user) {
        console.log(user);
        this.users.push(user);
        console.log(this.users);
    }

    getUsers() {
        return this.users;
    }

    deleteMsg() {
        this.getUser;
        return this.getUser;
    }

    hide() {


        this.visible = false;
    }

    show() {

        this.visible = true;
    }




}